from django.apps import AppConfig


class ReduxMusicConfig(AppConfig):
    name = 'Redux_Music'
