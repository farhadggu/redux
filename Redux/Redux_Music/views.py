from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.core.paginator import Paginator
from .models import Music, Album
from django.views.generic import ListView
from django.db.models import Q



def home(request):
    context = {
        'musics': Music.objects.all().order_by('-id')[:5],
        'albums': Album.objects.all()[:5],
        'overview': get_object_or_404(Album.objects.overview()),
    }
    return render(request, 'home_page.html', context)


def music_list(request, page=1):
    music = Music.objects.all().order_by('-id')
    paginator = Paginator(music, 10)
    musics = paginator.get_page(page)
    context = {'musics': musics}
    return render(request, 'view_all_song.html', context)


def download(request, path):
    file_path=os.path.join(settings.MEDIA_ROOT, path)
    if os.path.exists(file_path):
        with open(file_path, 'rb')as fh:
            response = HttpResponse(fh.read(), content_type="application/download")
            response['Content-Disposition']='inline;filename='+os.path.basename(file_path)
            return response
    raise Http404


def album(request, slug):
    context = {
        'album': get_object_or_404(Album, slug=slug)
    }
    return render(request, 'detail_album.html', context)


def album_list(request, page=1):
    album = Album.objects.all().order_by('-id')
    paginator = Paginator(album, 10)
    albums = paginator.get_page(page)
    context = {'albums': albums}
    return render(request, 'view_all_album.html', context)


def detail(request, slug):
    context = {
        'detail': get_object_or_404(Music, slug=slug)
    }
    return render(request, 'detail_song.html', context)


class SearchList(ListView):
    paginate_by = 10
    template_name = 'search_list.html'

    def get_queryset(self):
        search = self.request.GET.get('q')
        or_lookup = (Q(title__icontains=search) | Q(desc__icontains=search))
        return Music.objects.filter(or_lookup)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['search'] = self.request.GET.get('q')
        return context
