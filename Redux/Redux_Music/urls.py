from django.urls import path
from django.conf.urls import url
from django.views.static import serve
from Redux import settings
from .views import home, album, detail, music_list, album_list, SearchList

app_name = 'music'
urlpatterns = [
    path('', home, name='home'),
    url(r'^download/(?P<path>.*)$',serve,{'document_root':settings.MEDIA_ROOT}),
    path('detail-album/<slug:slug>', album, name='album'),
    path('detail-song/<slug:slug>', detail, name='detail'),
    path('all-music/page/<int:page>', music_list, name="music-list"),
    path('all-albums/page/<int:page>', album_list, name='album-list'),
    path('search/', SearchList.as_view(), name="search"),
    path("search/page/<int:page>", SearchList.as_view(), name="search")
]
