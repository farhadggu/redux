from django.db import models
from django.utils import timezone


#============My Manger================
class MusicManager(models.Manager):
    def overview(self):
        return self.filter(status=True)


class Album(models.Model):
    title = models.CharField(max_length=200, verbose_name='Album Name')
    desc = models.TextField(verbose_name='Artists Names')
    img = models.ImageField(upload_to="media", verbose_name='Image')
    slug = models.SlugField(max_length=200, verbose_name='Title in URL')
    status = models.BooleanField(default=False, verbose_name="Overview")
    position = models.IntegerField()

    def __str__(self):
        return self.title
    

    objects = MusicManager()


class Music(models.Model):
    title = models.CharField(max_length=100, verbose_name='Artist Name')
    desc = models.TextField(verbose_name='Music Name')
    slug = models.SlugField(max_length=100, verbose_name='Title in URL')
    img = models.ImageField(upload_to='media', verbose_name='Image')
    img2 = models.ImageField(upload_to='media', verbose_name='Image Slider', null=True, blank=True)
    album = models.ManyToManyField(Album, related_name="articles", null=True, blank=True)
    download = models.FileField(upload_to='media', verbose_name='Upload Download File', null=True, blank=True)
    publish = models.DateField(default=timezone.now, verbose_name='Publish Date')
    status = models.BooleanField(default=False, verbose_name="Overview")


    class Meta:
        verbose_name = "Music"
        verbose_name_plural = "Musics"


    def __str__(self):
        return self.title
    
    objects = MusicManager()

