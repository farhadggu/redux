from django.contrib import admin
from .models import Music, Album


class MusicAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'title', 'slug', 'publish', 'status']
    prepopulated_fields = {'slug':('desc',)}
    ordering = ['-status', 'publish']


admin.site.register(Music, MusicAdmin)


class AlbumAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'title', 'slug', 'status']
    prepopulated_fields = {'slug':('title',)}
    ordering = (['-id'])

admin.site.register(Album, AlbumAdmin)



