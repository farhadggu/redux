from django.apps import AppConfig


class ReduxAccountConfig(AppConfig):
    name = 'Redux_Account'
